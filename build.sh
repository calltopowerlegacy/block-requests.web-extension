rm -r build
zip -r -FS block-requests-web-extension.zip * -x "build.sh" "**/.DS_Store" "dist/" "dist/**"
mkdir build
mv block-requests-web-extension.zip build/
