/**
 * Block Requests - Web Extension
 * 
 * (c) Copyright 2017 Denis Meyer. All rights reserved.
 */
(function () {

    /**
     * Check and set a global guard variable.
     * If this content script is injected into the same page again, it will do nothing next time.
     */
    if (window.hasRun) {
        return;
    }
    window.hasRun = true;

    let blockList = [];

    function listener(requestDetails) {
        const blockingResponse = {};
        for (let urlI in blockList) {
            if (requestDetails.url === blockList[urlI]) {
                blockingResponse.cancel = true;
                break;
            }
        }

        return blockingResponse;
    }

    browser.webRequest.onBeforeRequest.addListener(
        listener, {
            urls: ["<all_urls>"]
        }, ["blocking"]
    );

    function handleMessage(request, sender, sendResponse) {
        if (request.type === 'urls') {
            blockList = request.data;
            console.log("Updated the block list:", blockList);
        }
    }

    browser.runtime.onMessage.addListener(handleMessage);

})();