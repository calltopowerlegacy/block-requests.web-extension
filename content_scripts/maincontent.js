/**
 * Block Requests - Web Extension
 * 
 * (c) Copyright 2017 Denis Meyer. All rights reserved.
 */
(function () {

    /**
     * Check and set a global guard variable.
     * If this content script is injected into the same page again, it will do nothing next time.
     */
    if (window.hasRun) {
        return;
    }
    window.hasRun = true;

    function sendMessage(msg) {
        function handleError(error) {
            console.log(`Error: ${error}`);
        }

        var sending = browser.runtime.sendMessage(msg);
        sending.then((response) => {}, handleError);
    }

    (function init() {
        function setUrls(result) {
            sendMessage({
                type: 'urls',
                data: result.urls.split('\n')
            });
        }

        function onError(error) {
            console.log(`Error: ${error}`);
        }

        var getting = browser.storage.local.get('urls');
        getting.then(setUrls, onError);
    })();

})();