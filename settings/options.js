function saveOptions(e) {
    e.preventDefault();
    browser.storage.local.set({
        urls: document.querySelector("#urls").value
    });
}

function restoreOptions() {
    function setUrls(result) {
        document.querySelector('#urls').value = result.urls || '';
    }

    function onError(error) {
        console.log(`Error: ${error}`);
    }

    var getting = browser.storage.local.get('urls');
    getting.then(setUrls, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);