# Block requests - Web Extension

Blocks certain web requests.

## Testing

* Go to about:debugging
* Alternatively
  * install web-ext via node
  * run `web-ext run`

## Building

* Use the build script
  * run `./build.sh`

## Distribution

* Sign the extension at https://addons.mozilla.org/en-US/developers
